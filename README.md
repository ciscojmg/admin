# 🎟 ADMIN

## Panel de control
- Tramas Xbee
- Comandos AT
- Debug
- Comando con tarjeta FeederAT

## Install
  - `npm install`
  - `npm run dev`

## Multimedia
![admin](/uploads/d8ecd20b06d61aef966b375e31c8a2ee/admin.png)
