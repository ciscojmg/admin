// express
const express = require('express');
const http = require('http');
const path = require('path');
const app = express();

// serial port
const SerialPort = require('serialport');

// socketio
const SocketIo = require('socket.io');

//xbee
var xbee_api = require('xbee-api');
var xbeeAPI = new xbee_api.XBeeAPI({
    api_mode: 1
});


//settings
app.set('port', 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

const server = http.createServer(app);
const io = SocketIo.listen(server);

//port
// create serial port
var portName = 'COM52'; 
const port = new SerialPort(portName, {
    baudRate: 9600,
    parity: 'none',
    dataBits: 8,
    stopBits: 1
});
port.pipe(xbeeAPI.parser);


// middlewares
// traduce los datos que llegan de un formulario y los coloca en la propiedad body
app.use(express.urlencoded({extended: false}));

// routes
app.use(require('./routes/'));


//open serial port
port.on('open', function () {
    console.log('Opened Port.');
});

// All frames parsed by the XBee will be emitted here
xbeeAPI.parser.on("data", function(frame) {
    let xbeeFrame = [];
    // console.log(">>", frame.data);
    xbeeFrame = frame.data.toString();
    console.log(">>", xbeeFrame, "<<");
    io.emit('xbee:data', {
        value: xbeeFrame
    });

});

//error serial port
port.on('err', function (data) {
    console.log(err.message);
});

// static files
app.use(express.static(path.join(__dirname, 'public')))

//server listen
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});