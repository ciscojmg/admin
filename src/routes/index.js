const express = require('express');
const router = express.Router();
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

//leer archivo devices
const jsonDevices = fs.readFileSync('src/database/devices.json', 'utf-8');

//convertirlo a string
let devices = JSON.parse(jsonDevices);

router.get('/', (req, res) => {
    res.render('index.html', {
        title: 'bioFeeder | Dashboard',
        devices
    });
});

router.get('/commandat', (req, res) => {
    res.render('commandat.html', {
        title: 'bioFeeder | Comando AT',
        devices
    });
});

router.get('/devices', (req, res) => {
    res.render('devices.html', {
        title: 'bioFeeder | Devices',
        devices
    });

});

router.post('/devices', (req, res) => {
    //console.log(req.body);
    const { nameDevices } = req.body;

    let newDevices = {
        id: uuidv4(),
        nameDevices
    }

    devices.push(newDevices);

    pathVars = "src/database/vars/" + newDevices.id + ".json";
    pathTest = "src/database/test/" + newDevices.id + ".json";

    const jsonDevices = JSON.stringify(devices);

    fs.writeFileSync('src/database/devices.json', jsonDevices, 'utf-8');
    fs.writeFileSync(pathVars, "[{}]", 'utf-8');
    fs.writeFileSync(pathTest, "[{}]", 'utf-8');


    res.redirect('/devices');
});

router.get('/deleteDevices/:id', (req, res) => {

    const { id } = req.params;
    //console.log(id);

    pathVars = "src/database/vars/" + id + ".json";
    pathTest = "src/database/test/" + id + ".json";

    console.log(pathVars);

    devices = devices.filter(devices => devices.id != req.params.id);
    const jsonDevices = JSON.stringify(devices);
    fs.writeFileSync('src/database/devices.json', jsonDevices, 'utf-8');


    
    fs.unlink(pathVars, function (err) {
        if (err) return console.log(err);
        // console.log('deleted Vars successfully');
    });

    fs.unlink(pathTest, function (err) {
        if (err) return console.log(err);
        // console.log('deleted Test successfully');
    });
    

    res.redirect('/devices');

});

router.get('/settings', (req, res) => {
    res.render('settings.html', {
        title: 'bioFeeder | Settings',
        devices
    });
});

router.get('/utilities', (req, res) => {
    res.render('utilities.html', {
        title: 'bioFeeder | Utilidades',
        devices
    });
});

module.exports = router;